/*
    @Author: Furkan Basaran <frknbasaran@gmail.com>
    @Date: 24.12.2015
    @File: /app.js
*/

/*
    Call module dependencies
*/
var express = require('express');
var path    = require('path');
var bodyParser = require('body-parser');

/*
    Define express app
*/
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'build')));

/*
    Handle exported routers from routes folder
*/
var routes = require('./routes/index');
var posts  = require('./routes/posts');

/*
    Define application routes
*/
app.use('/', routes);
app.use('/posts', posts);


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
