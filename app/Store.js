var flux = require('flux-react');
var actions = require('./actions.js');
var _   = require('underscore');
var $ = require('jquery');
var config = require('./config');

module.exports = flux.createStore({
  posts: [],
  actions: [
    actions.addPost,
    actions.setPosts,
    actions.editPost
  ],

  addPost: function (post) {

    var that = this;

    $.ajax({
      type: "POST",
      url : config.serviceURL + "/posts",
      data: "headerText=" + post.headerText + "&bodyText=" + post.bodyText,
      header: {'Content-Type':'application/x-www-form-urlencoded'}
    })
      .then(function successCallback(res) {
          if (!res.error) {
            that.posts.unshift(res.data);
            that.emitChange();
          } else console.error(res.message)
      })

  },

  editPost: function(post) {
    
    _.each(this.posts, function(singlePost){ 
        if (singlePost.id == post.id) {
            singlePost.bodyText   = (post.bodyText == "") ? singlePost.bodyText : post.bodyText;
            singlePost.headerText = (post.headerText == "") ? singlePost.headerText : post.headerText;
        }
    }); 

    this.emitChange();

  },
  setPosts: function (posts) {

    this.posts = posts;
    
    this.emitChange();

  },
  exports: {
    getPosts: function () {

      var that = this;

      $.ajax({
        type: "GET",
        url : config.serviceURL + "/posts"
      })
          .then(function successCallback(res) {
            if(!res.error) {
              that.posts = res.data;
              that.emitChange();
            } else console.error(res.message);
          })

      return this.posts;
    },
    removePost: function (post) {
  
        var that = this;
  
        $.ajax({
          type: "DELETE",
          url : config.serviceURL + "/posts/"+post.id
        })
          .then(function successCallback(res) {
              if (!res.error) {
                that.posts = _.without(that.posts, post);
                that.emitChange();
              } else console.error(res.message)
          })
      
    }
  }
});
