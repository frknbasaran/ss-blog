var flux = require('flux-react');

module.exports = flux.createActions([
  'addPost','removePost','setPosts','editPost'
]);