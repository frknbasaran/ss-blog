var React = require('react');
var Store = require('./Store.js');
var actions = require('./actions.js');
var moment = require('moment');

var App = React.createClass({

    getInitialState: function () {
        return {
            posts: Store.getPosts(),
            newPost: {
                headerText: '',
                bodyText: ''
            }
        };
    },

    componentWillMount: function () {
        console.info("App component will mount")
        Store.addChangeListener(this.changeState);
    },

    componentWillUnmount: function () {
        Store.removeChangeListener(this.changeState);
    },
    
    changeState: function () {
        this.setState({
            posts: Store.getPosts()
        });
    },

    addPost: function (event) {
        event.preventDefault();
        
        var headerText = this.refs.headerText.getDOMNode().value;
        
        var bodyText = this.refs.bodyText.getDOMNode().value;
        
        actions.addPost({headerText: headerText, bodyText: bodyText});
        
    },

    updateNewPost: function (event) {
        this.setState({
            newPost: {
                'headerText': this.refs.headerText.getDOMNode().value,
                'bodyText': this.refs.bodyText.getDOMNode().value
            }
        });
    },

    removePost: function (post) {
        Store.removePost(post);
    },
    
    printLog: function () {
      console.log("LOG");
    },
    
    renderPosts: function (post) {
        var that = this;
        return (
            <div className='post'>
                <div className='pull-left'><h4 class="header">{post.headerText}</h4></div>
                <div className='pull-right date'><i>{moment(post.created_at).fromNow()}</i></div>
                <div className='clearfix'></div>
                <div className='postContent'>
                    <p>{post.bodyText}</p>
                </div>
                <div className='buttons pull-right'>
                    <button key="{post.id}" onClick={that.removePost.bind(that, post)} className='none-border'><i className='fa fa-trash'></i> Remove</button>
                    <button onClick={that.printLog} className='none-border'><i className='fa fa-edit'></i> Edit</button>
                </div>
                <div className='clearfix'></div>
            </div>
        );
    },

    render: function () {
        return (

            <div>
                { this.state.posts.length < 1 &&
                  <div className="notification"><h4>Hey!</h4><p>Not found any post to show.</p></div>
                }
                  {this.state.posts.map(this.renderPosts)}
                <div className='post-form'>
                    <h4 className='sub-header'>Create a new post</h4>

                    <form onSubmit={this.addPost}>
                        <div className='form-field'>
                            <label>Header</label>
                            <input placeholder='A header for new post' className='form-input' ref="headerText"
                                   type="text"
                                   required value={this.state.newPost.headerText}
                                   onChange={this.updateNewPost}/>
                        </div>
                        <div className='form-field'>
                            <label>Post Content</label>
                    <textarea placeholder='Lorem ipsum dolor sim amed...' className='form-input' ref="bodyText"
                              type="text" required value={this.state.newPost.bodyText}
                              onChange={this.updateNewPost}></textarea>
                        </div>
                        <div className='form-field'>
                            <div className='pull-right'>
                                <input className='button' type="submit" value="Add Post"/>
                            </div>
                            <div className='clearfix'></div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }

});

module.exports = App;
