var express  = require('express');
var router   = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.json({"error":true, "message":"Read api docs.","data":[]});
});

module.exports = router;
