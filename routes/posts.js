/*
    @Author: Furkan Basaran <frknbasaran@gmail.com>
    @Date: 24.12.2015
    @File: /routes/posts.js
*/

/*
    Define module dependencies
*/
var express  = require('express');
var router   = express.Router();
var jsonfile = require('jsonfile');
var _        = require('underscore');
var warder   = require('warder');

/*
    Call a json file as datastore
*/
var file = 'data.json';

/*
    GET /posts
    Return all posts
*/
router.get('/', function(req, res) {
    
    var response = {};
    
    jsonfile.readFile(file, function(err, data) {
        
        if (!err) response = {"error":false, "message":"","data":data.posts}
        else response = {"error":true, "message":err,"data":[]};
        
        res.json(response);
        
    })
    
});

/*
    GET /posts/:id
    Return post by specific id
*/
router.get('/:id', function(req, res) {
    
    jsonfile.readFile(file, function(err, data) {
    
         var response = {};
    
         if(!err) {
    
            var postByID = _.find(data.posts, function(post){ return post.id == req.params.id; });
         
            if (typeof postByID == "undefined") response = {"error":true, "message":"Not found matched post with this id.","data":{}};
            else response = {"error":false, "message":"","data":postByID};    
    
         } else response = {"error":true, "message":err,"data":{}};    
    
         res.json(response);
         
    })
})

/*
    POST /posts
    Create new post
    @bodyParam String headerText
    @bodyParam String bodyText
*/
router.post('/', warder('body', ['bodyText','headerText']), function(req, res) {
    
    console.log(req.body.headerText)
    console.log(req.body.bodyText)
    
    jsonfile.readFile(file, function(err, data) {
        
        var response = {};
        var increment = ++data.lastPostID;
    
        if(req.body.headerText.length > 0 && req.body.bodyText.length > 0) {
    
            var post = {"id":increment, "headerText":req.body.headerText, "bodyText":req.body.bodyText, "created_at":Date.now()};
        
            data.posts.unshift(post);
            
            jsonfile.writeFile(file, data, function(err) {
            
                if(!err) response = {"error":false, "message":"","data":post};
                else response = {"error":true,"message":err,"data":{}};
            
                res.json(response);
                
            })    
            
        } else res.json({"error":true, "message":"You can't pass empty header and body params."})
        
    })
    
})

/*
    PUT /posts/:id
    Update post which speficied id with data sent from client
    @bodyParam String headerText
    @bodyParam String bodyText
*/
router.put('/:id', function(req, res) {
    
    jsonfile.readFile(file, function(err, data) {
        
        var response = {};
        
        var postByID = _.find(data.posts, function(post){ return post.id == req.params.id; });
        
        if (typeof postByID == "undefined") response = {"error":true, "message":"Not found matched post with this id.","data":{}};
        else {
        
            postByID.headerText = req.body.headerText || postByID.headerText;
            postByID.bodyText = req.body.bodyText || postByID.bodyText;
            postByID.updated_at = Date.now();
            
            _.each(data.posts, function(post){ 
                if (post.id == req.params.id) {
                    post.headerText = postByID.headerText;
                    post.bodyText   = postByID.bodyText;
                }
            });            
            
            jsonfile.writeFile(file, data, function(err) {
                
                if(!err) response = {"error":false, "message":"","data":postByID};
                else response = {"error":true,"message":err,"data":{}};
                
                res.json(response);
                
            })  
            
        }
    })
    
})

/*
    DELETE /posts/:id
    Remove post by id
*/
router.delete('/:id', function(req, res) {
    
    jsonfile.readFile(file, function(err, data) {
        
        var postByID = _.find(data.posts, function(post){ return post.id == req.params.id; });
        var removed  = _.without(data.posts, postByID)
        
        data.posts = removed;
        
        jsonfile.writeFile(file, data, function(err) {
            
            if(!err) response = {"error":false, "message":"Removed successfully","data":{}};
            else response = {"error":true,"message":err,"data":{}};
            
            res.json(response);
            
        })  
        
    })
    
})



module.exports = router;
